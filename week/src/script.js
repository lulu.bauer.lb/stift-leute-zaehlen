'use strict';

console.log('script.js is working');

//Current Date
let today = new Date();
let date = today.getDate()+'.'+(today.getMonth()+1)+'.'+today.getFullYear();
document.querySelector('#date').innerHTML = date;

//console.log(Cookies.get('p2_10_00'));

//Get Cookies from Browser
let result1000 = document.querySelector('#result_10_00');
if(Cookies.get('10_00') == undefined){
    result1000.innerHTML = '0';
} else {
    result1000.innerHTML = Cookies.get('10_00');
}
let result1130 = document.querySelector('#result_11_30');
if(Cookies.get('11_30') == undefined){
    result1130.innerHTML = '0';
} else {
    result1130.innerHTML = Cookies.get('11_30');
}
let result1330 = document.querySelector('#result_13_30');
if(Cookies.get('13_30') == undefined){
    result1330.innerHTML = '0';
} else {
    result1330.innerHTML = Cookies.get('13_30');
}
let result1515 = document.querySelector('#result_15_15');
if(Cookies.get('15_15') == undefined){
    result1515.innerHTML = '0';
} else {
    result1515.innerHTML = Cookies.get('15_15');
}
let result1630 = document.querySelector('#result_16_30');
if(Cookies.get('16_30') == undefined){
    result1630.innerHTML = '0';
} else {
    result1630.innerHTML = Cookies.get('16_30');
}

function changePerson(time){
    let string = "#result_" + time;
    let result = document.querySelector(string).innerHTML;
    let parsedResult = parseInt(result);
    let persons = document.querySelector('#persons').value;
    let parsedPersons = parseInt(persons);
    let addPersons = parsedResult + parsedPersons;
    let cookieName = time;
    Cookies.set(cookieName, addPersons, { expires: 1});
    document.querySelector(string).innerHTML = Cookies.get(cookieName);
    location.reload();
}

function back(time){
    let string = "result_" + time;
    let result = document.querySelector(string).innerHTML;
    let parsedResult = parseInt(result);
    let change = parsedResult - 1;
    let cookieName = time;
    Cookies.set(cookieName, change, {expires: 1});
    document.querySelector(string).innerHTML = Cookies.get(cookieName);
    location.reload();
}
